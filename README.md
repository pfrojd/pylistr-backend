# Pylistr

## How to start

Brief instructions, will add more later.

    $ pip install -r requirements.txt

-   Make your own `.env` file from the template, set up postgreSQL etc.
-   Install Python related stuff for VSCode.
-   Run database migrations to get up to speed (see comments in `application.py`)
-   Run seeds to get up to speed `flask seed`
-   Run application with `python app.py`

## Big TODO

-   Fix refreshing of JWT tokens etc, they are infinite right now
    -   Or is it a problem?
    -   https://flask-jwt-extended.readthedocs.io/en/latest/api.html#flask_jwt_extended.JWTManager.invalid_token_loader
-   ~~Add support for comments~~
-   ~~Add support for lists having multiple users, right now there is only an "owner"~~
-   ~~Implement either E2E tests or Unit Tests~~
-   Clean up
-   Add Swagger/OpenAPI support
-   Forgot my password/Reset password features
-   Add support for jobs?
    -   Update Steam games etc.
    -   Consider using `octopus` or something for multiple threaded requests
    -   `schedule` looks good for this.
-   Go through requirements and remove crap.
-   Optimize `sqlalchemy` related stuff
    -   Add backrefs?
    -   Use lazyloading?
    -   ~~Verify there's proper cascades on all things.~~
        -   ~~FIX CASCADES~~
    -   Consider indexes?
-   Clean up schemas (Marshmallow)
    -   There's probably better ways of having multiple versions of one schema

### Upcoming features?

-   Review process of how users join lists.
    -   Provide some sort of token system
        -   Perhaps have a central table with tokens and token types.
        -   key: x, token_type: INVITE_TO_LIST
    -   An owner of a list should be able to invite others to lists by either providing their name or by giving them a link.
        -   Requirements to join a list:
            -   User is not already in it.
            -   Inviter has permissions.
-   ~~Add support for `user` to have their SteamID.~~
-   Add events
    -   Both events tied to games (such as releases, upcoming leagues, patches etc)
    -   Also generic events like steam sales.
-   ~~Add genre and category support~~
    -   ~~Read genres, tags and categories from Steam, and mirror these onto our own system.~~
-   Save less data into \$json for steam games.
-   Add slugs to games
    -   Use slugs to query for games instead of id (or both)
    -   ~~Columns to save:~~
        -   ~~`is_free`~~
        -   ~~`platforms`?~~
        -   ~~metacritic score?~~
        -   ~~recommendations~~
    -   Build a basic list of games to have:
        -   Look at DLCs/Expansions (Grim Dawn as an example)
        -   Look at Free to Play games (Secret World/Warframe/Path of Exile)
        -   Look at Early Access games (?)
        -   Look at AAA games.
        -   Make sure we can handle all types
-   Add game reviews into the mix.
    -   https://partner.steamgames.com/doc/store/getreviews
    -   Save them, and perhaps crawl them at the same time as price is updated.
-   Add game news separately

    -   Maybe only get them live when viewing pages?
    -   https://api.steampowered.com/ISteamNews/GetNewsForApp/v2

-   Convert GeorgeBotts to Python.
    -   No, but build some sort of simplistic messaging system through the database.
        -   Pylistr will post events to a queue, which is regularly polled by the discord bot.
        -   Discord bot will post a message to a channel, then clear that item off the queue.
    -   Build up a database of upcoming Steam releases using their API.
    -   Crawl every single non-released game (should be doable?)
    -   Save the dates of upcoming releases.
    -   Set up a recurring crawler that will update upcoming games release dates.

## Inspiration and stuff

-   https://github.com/miguelgrinberg/microblog/
-   https://realpython.com/token-based-authentication-with-flask/
-   https://github.com/gothinkster/flask-realworld-exampl
-   https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

## Links for JWT

https://flask-jwt-extended.readthedocs.io/en/latest/api.html#module-flask_jwt_extended
https://flask-jwt-extended.readthedocs.io/en/latest/index.html
https://flask-jwt-extended.readthedocs.io/en/latest/refresh_tokens.html
https://flask-jwt-extended.readthedocs.io/en/latest/token_freshness.html
https://flask-jwt-extended.readthedocs.io/en/latest/add_custom_data_claims.html
https://flask-jwt-extended.readthedocs.io/en/latest/tokens_from_complex_object.html
