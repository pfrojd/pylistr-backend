import pytest
import os
import json
from server.extensions import db as _db
from server.application import create_app

from server.model.role import Role
from server.model.user import User

from alembic.command import upgrade
from alembic.config import Config


ALEMBIC_CONFIG = "{}/migrations/alembic.ini".format(os.getcwd())


def apply_migrations():
    """Applies all alembic migrations."""
    print(os.getcwd())
    config = Config(ALEMBIC_CONFIG)
    upgrade(config, "head")


"""
    ALWAYS consume this fixture to make sure you have
    a test_client to consume (so that you can use the endpoints)
"""


@pytest.fixture(scope="session")
def app(request):
    app = create_app("server.config.TestConfig")

    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope="session")
def db(app, request):
    def teardown():
        _db.drop_all()

    _db.create_all()
    """
        Add additional seed data here, like standard users
    """
    user = User("test-user", "foo@a.com", "123")
    second_user = User("second-user", "foo@b.com", "123")
    _db.session.add(Role("Owner"))
    _db.session.add(Role("Regular"))
    _db.session.add(user)
    _db.session.add(second_user)
    _db.session.commit()
    request.addfinalizer(teardown)
    return _db


"""
    Only consume this directly if you know you're going
    to need to communicate with the database straight up.

    Also consume this in other fixtures if you wish to interact
    with the database in the fixture.

    Note: All transactions done with this fixture will be rolled back
"""


@pytest.fixture(scope="function")
def session(app, db, request):
    """Creates a new database session for a test."""
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    def teardown():
        transaction.rollback()
        connection.close()
        session.remove()

    request.addfinalizer(teardown)
    return session


"""
    Consume this fixture to make sure you have a valid JWT
    token for when communicating with the APIs
"""


@pytest.fixture(scope="function")
def auth(app, session):
    client = app.test_client()
    url = "/api/auth/login"
    user_data = {"email": "foo@a.com", "password": "123"}
    response = client.post(
        url, data=json.dumps(user_data), content_type="application/json"
    )
    assert response.status_code == 200
    return json.loads(response.data)["auth_token"]


"""
    Consume this fixture to make sure you have a second
    user to test for permission related things.
"""


@pytest.fixture(scope="function")
def second_user(app, session):
    client = app.test_client()
    url = "/api/auth/login"
    user_data = {"email": "foo@b.com", "password": "123"}
    response = client.post(
        url, data=json.dumps(user_data), content_type="application/json"
    )
    assert response.status_code == 200
    token = json.loads(response.data)["auth_token"]

    response = client.get(
        "/api/auth/me",
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(token)},
    )
    assert response.status_code == 200
    id = json.loads(response.data)["logged_in_as"]

    return token, id


"""
    Consume this fixture to make sure you have a list
    before running your test.
"""


@pytest.fixture(scope="function")
def list(app, session):
    client = app.test_client()
    url = "/api/auth/login"
    user_data = {"email": "foo@a.com", "password": "123"}
    response = client.post(
        url, data=json.dumps(user_data), content_type="application/json"
    )
    assert response.status_code == 200
    response_json = json.loads(response.data)
    assert response_json["auth_token"] is not None

    token = response_json["auth_token"]
    response = client.post(
        "/api/list/create",
        json={"name": "Intern Testing List"},
        headers={"Authorization": "Bearer {}".format(token)},
    )

    assert response.status_code == 201
    response_json = json.loads(response.data)
    id = response_json["id"]
    slug = response_json["slug"]
    assert id is not None

    return id, slug


"""
    Consume this fixture to make sure you have a game_entry
    before running your test.
"""


@pytest.fixture(scope="function")
def game_entry(app, auth, list):
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/{}".format(list_slug, 261550)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200
    id = json.loads(response.data)["id"]
    return id
