# -*- coding: utf-8 -*-
"""
    Should probably be removed in favor of tests for the `game` routes.

    Tests the Steam-service, to make sure we can get wishlists, and add games.
"""

from server.service.steam import Steam

my_profile_id = 76561198039973392
test_game_id = 261550


def test_get_wishlist():
    wishlist_games = Steam.get_wishlist(my_profile_id)
    assert len(wishlist_games) > 0


def test_add_game_from_steam(app, auth):
    client = app.test_client()
    url = "/api/game/add/{}".format(test_game_id)
    response = client.get(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 200
