# -*- coding: utf-8 -*-
"""
    Integration tests for the `server.application.routes.game_entry` routes.
    As more endpoints are added, more tests should be added.
"""

import json

"""
    Add comments to game entries
"""


def test_add_comment_to_game_entry(app, auth, list, game_entry):
    client = app.test_client()
    url = "/api/game_entry/{}/comment".format(game_entry)
    data = {"message": "this is a test comment"}
    response = client.post(
        url, json=data, headers={"Authorization": "Bearer {}".format(auth)}
    )
    assert response.status_code == 200


def test_add_comment_to_missing_game_entry(app, auth, list, game_entry):
    client = app.test_client()
    url = "/api/game_entry/{}/comment".format(game_entry + 1)
    data = {"message": "this is a test comment"}
    response = client.post(
        url, json=data, headers={"Authorization": "Bearer {}".format(auth)}
    )
    assert response.status_code == 404


"""
    Add ratings to game entries
"""


def test_add_rating_to_game_entry(app, auth, game_entry):
    client = app.test_client()
    url = "/api/game_entry/{}/rate/{}".format(game_entry, 5)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200
    rating = json.loads(response.data)
    assert rating["rating"] == 5


def test_update_rating_to_game_entry(app, auth, game_entry):
    client = app.test_client()
    url = "/api/game_entry/{}/rate/{}".format(game_entry, 5)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200
    rating = json.loads(response.data)
    assert rating["rating"] == 5

    url = "/api/game_entry/{}/rate/{}".format(game_entry, 3)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})

    assert response.status_code == 200
    rating = json.loads(response.data)
    assert rating["rating"] == 3
