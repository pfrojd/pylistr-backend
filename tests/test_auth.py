# -*- coding: utf-8 -*-
"""
    Integration tests for the `server.application.routes.auth` routes.
    As more endpoints are added, more tests should be added.
"""

import random
import json

random_id = random.randint(1, 100000)

"""
    Register user
"""


def test_register_user(app, session):
    client = app.test_client()
    url = "/api/auth/register"
    user_data = {
        "username": "foo{}".format(random_id),
        "email": "foo{}".format(random_id),
        "password": "123",
    }
    response = client.post(
        url, data=json.dumps(user_data), content_type="application/json"
    )
    assert response.status_code == 200


"""
    Log a user in
"""


def test_login_user(app, session):
    client = app.test_client()
    url = "/api/auth/login"
    user_data = {"email": "foo@a.com", "password": "123"}
    response = client.post(
        url, data=json.dumps(user_data), content_type="application/json"
    )
    assert response.status_code == 200


"""
    Get the users id
"""


def test_me(app, auth):
    client = app.test_client()
    url = "/api/auth/me"
    response = client.get(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 200

