# -*- coding: utf-8 -*-
"""
    Integration tests for all of the routes in `server.application.routes.list`
    As more endpoints are added, more tests should be added.
"""

test_game_id = 261550
test_game_slug = "grim-dawn"

"""
    Get all lists
"""


def test_get_all_lists(app, auth, list):
    client = app.test_client()
    url = "/api/list/"
    response = client.get(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 200


"""
    Get one list
"""


def test_get_one_list(app, auth, list):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}".format(list_slug)
    response = client.get(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 200


def test_get_one_list_fail(app, auth, list):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}".format(list_slug + "1")
    response = client.get(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 404


"""
    Deleting lists
"""


def test_delete_one_list(app, auth, list):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}".format(list_id)
    response = client.delete(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 200


def test_delete_one_list_fail(app, auth, list):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}".format(list_id + 1)
    response = client.delete(
        url,
        content_type="application/json",
        headers={"Authorization": "Bearer {}".format(auth)},
    )
    assert response.status_code == 404


"""
    Create lists
"""


def test_create_one_list(app, auth):
    client = app.test_client()
    url = "/api/list/create"
    data = {"name": "Intern Testing List Two"}
    response = client.post(
        url, json=data, headers={"Authorization": "Bearer {}".format(auth)}
    )
    assert response.status_code == 201


def test_create_one_list_fail(app, auth, list):
    client = app.test_client()
    url = "/api/list/create"
    data = {"name": "Intern Testing List"}
    response = client.post(
        url, json=data, headers={"Authorization": "Bearer {}".format(auth)}
    )
    assert response.status_code == 400


"""
    Add users to list
"""


def test_add_user_to_list(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    # Assert the user can actually see the list
    url = "/api/list/{}".format(list_slug)
    response = client.get(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 200


def test_add_user_to_list_missing_user(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id + 1)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 404


def test_add_user_to_list_missing_permissions(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    # Add the second user to the list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    # As the second user, attempt to add a user
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, 0)
    response = client.post(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 401


def test_add_user_to_missing_list(app, auth, second_user):
    second_user_token, second_user_id = second_user
    # Add the second user to the list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format("empty", second_user_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 404


"""
    Add games to list
"""


def test_add_game_to_list(app, list, auth):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}/{}".format(list_slug, test_game_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200


def test_add_game_to_list_already_added(app, list, auth):
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}/{}".format(list_slug, test_game_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 204


def test_add_game_to_missing_list(app, list, auth, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/{}".format(list_slug + "1", test_game_id)
    response = client.post(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 404


def test_add_game_to_list_missing_permissions(app, list, auth, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/{}".format(list_slug, test_game_id)
    response = client.post(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 404


def test_add_nonexisting_game_to_list(app, list, auth):
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/{}".format(list_slug, 0)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 400


"""
    Removing users from list
"""


def test_remove_from_list(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    # Add them to the list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    # Remove them from the list
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id)
    response = client.delete(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    # Assert the user can't see the list after being deleted
    url = "/api/list/{}".format(list_slug)
    response = client.get(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 404


def test_remove_missing_user_from_list(app, auth, list, second_user):
    list_id, list_slug = list
    second_user_token, second_user_id = second_user
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id + 1)
    response = client.delete(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 404


def test_remove_from_list_missing_permissions(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    # Add the second user to the list
    client = app.test_client()
    list_id, list_slug = list
    url = "/api/list/{}/user/{}".format(list_slug, second_user_id)
    response = client.post(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 200

    # As the second user, attempt to add a user
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_slug, 0)
    response = client.delete(
        url, headers={"Authorization": "Bearer {}".format(second_user_token)}
    )
    assert response.status_code == 401


def test_remove_from_missing_list(app, auth, list, second_user):
    second_user_token, second_user_id = second_user
    list_id, list_slug = list
    client = app.test_client()
    url = "/api/list/{}/user/{}".format(list_id + 1, second_user_id)
    response = client.delete(url, headers={"Authorization": "Bearer {}".format(auth)})
    assert response.status_code == 404

