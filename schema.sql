-- user
CREATE TABLE "user" (
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    username varchar(256) not null,
    email varchar(256) not null,
    password varchar(512) not null,
    CONSTRAINT PK_User PRIMARY KEY (id)
);

-- list
CREATE TABLE list(
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    name varchar(256) not null,
    type integer not null,
    user_id integer not null,
    CONSTRAINT PK_List PRIMARY KEY (id),
    CONSTRAINT FK_List_OwnedByUser FOREIGN KEY(user_id) references "user" ON DELETE CASCADE
);

-- user_list (Junction)
CREATE TABLE user_list (
    user_id integer not null,
    list_id integer not null,
    CONSTRAINT PK_User_List PRIMARY KEY (user_id, list_id),
    CONSTRAINT FK_User_InList FOREIGN KEY(user_id) references "user" ON DELETE CASCADE,
    CONSTRAINT FK_List_InUser FOREIGN KEY(list_id) references "list" ON DELETE CASCADE
);

-- game
CREATE TABLE game (
    id SERIAL,
    steam_id varchar(256) not null,
    name varchar(256) not null,
    CONSTRAINT PK_Game PRIMARY KEY (id)
);

-- game_entry
CREATE TABLE game_entry(
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer not null,
    game_id integer not null,
    list_id integer not null,
    CONSTRAINT PK_GameEntry PRIMARY KEY (id),
    CONSTRAINT FK_GameEntry_CreatedByUser FOREIGN KEY(user_id) references "user" ON DELETE CASCADE,
    CONSTRAINT FK_GameEntry_ForGame FOREIGN KEY(game_id) references "game" ON DELETE CASCADE,
    CONSTRAINT FK_GameEntry_InList FOREIGN KEY(list_id) references "list" ON DELETE CASCADE
);

CREATE TABLE game_entry_rating (
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer not null,
    game_entry_id integer not null,
    rating integer not null,
    CONSTRAINT PK_GameEntryRating PRIMARY KEY (id),
    CONSTRAINT FK_GameEntryRating_ByUser FOREIGN KEY(user_id) references "user" ON DELETE CASCADE,
    CONSTRAINT FK_GameEntryRating_GameEntry FOREIGN KEY(game_entry_id) references "game_entry" ON DELETE CASCADE
);



-- comments
CREATE TABLE comment (
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer not null,
    game_entry_id integer not null,
    message text not null,
    CONSTRAINT PK_Comment PRIMARY KEY (id),
    CONSTRAINT FK_Comment_ByUser FOREIGN KEY(user_id) references "user" ON DELETE CASCADE,
    CONSTRAINT FK_Comment_OnGameEntry FOREIGN KEY(game_entry_id) references "game_entry" ON DELETE CASCADE
);

-- list_role
CREATE TABLE list_role (
    id SERIAL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    user_id integer not null,
    list_id integer not null,
    role_id integer not null,
    CONSTRIANT PK_List_Role PRIMARY KEY(id),
    CONSTRAINT FK_List_Role_User FOREIGN KEY(user_id) references "user" ON DELETE CASCADE,
    CONSTRAINT FK_List_Role_List FOREIGN KEY(list_id) references "list" ON DELETE CASCADE,
    CONSTRAINT FK_List_Role_Role FOREIGN KEY(role_id) references "role" ON DELETE CASCADE,
)

-- role
CREATE TABLE role (
    id SERIAL,
    role_name text not null,
    CONSTRIANT PK_Role PRIMARY KEY(id)
)


-- SEARCH RELATED STUFF

CREATE INDEX tsv_game_name_idx ON steam_game USING gin(search);

CREATE FUNCTION steam_game_text_search_trigger() RETURNS trigger AS $$
begin
  new.search :=
    to_tsvector(new.name);
return new;
end
$$ LANGUAGE plpgsql;

/* Trigger on update */
CREATE TRIGGER tsvector_game_name_update BEFORE INSERT OR UPDATE
ON steam_game FOR EACH ROW EXECUTE PROCEDURE steam_game_text_search_trigger();
