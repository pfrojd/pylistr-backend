# -*- coding: utf-8 -*-
"""
This is the configuration file for the application
This contains various flags and other stuff that's used by the frameworks we use.

It should not contain any sensitive information, passwords, usernames
and other database information should be added to the .env file.
"""

import os
from datetime import timedelta

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # flask core settings
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = os.environ["DATABASE_URI"]
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # JWT
    JWT_SECRET_KEY = os.environ["APP_SECRET"]
    JWT_AUTH_USERNAME_KEY = "email"


class ProductionConfig(Config):
    # Production settings (a.k.a deployed settings) go here
    DEBUG = False


class DevelopmentConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(10 ** 6)


class TestConfig(Config):
    DEBUG = True
    DEVELOPMENT = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(10 ** 6)
    SQLALCHEMY_DATABASE_URI = os.environ["TEST_DATABASE_URI"]
