
# -*- coding: utf-8 -*-
"""
    We can put some extension methods here, or other database related things.
    This isn't really necessary since we're sort of forced to put
    things like the CRUDMixin in `server/extensions.py`

    But the `FancyModel` is indeed used to make sure that all our models
    get the `id` column. Maybe we should add more to this?
"""
from server.extensions import db


class FancyModel(object):
    __table_args__ = {"extend_existing": True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, record_id):
        """Get record by ID."""
        if any(
            (
                isinstance(record_id, (str, bytes)) and record_id.isdigit(),
                isinstance(record_id, (int, float)),
            )
        ):
            return cls.query.get(int(record_id))

    @classmethod
    def get_by_slug(cls, record_slug):
        """ Get record by slug"""
        return cls.query.filter_by(slug=record_slug).first()
