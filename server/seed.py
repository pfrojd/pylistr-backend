# -*- coding: utf-8 -*-
"""
    Responsible for dealing with seeding data into the database.
    Typically, this kind of data should be bootstrapped before the
    application is started.

    To run this, simply run the `flask seed` command
"""
from flask_migrate import MigrateCommand
from server.extensions import db
from server.model.role import Role
from server.model.user import User
from server.model.game import Game
from server.service.steam import Steam

games_to_seed = [
    215280,
    243120,
    261550,
    268130,
    312670,
    313120,
    320240,
    323190,
    325210,
    327560,
    331480,
    342310,
    352720,
    359550,
    362960,
    375230,
    393380,
    396750,
    399810,
    402710,
    411300,
    412020,
    415590,
    427290,
    439340,
    445190,
    458000,
    460950,
    464920,
    466500,
    467760,
    482400,
    485510,
    489630,
    492720,
    493520,
    494430,
    517630,
    523660,
    524010,
    526160,
]


def seed_db():
    """
        Add additional seeding data here but mind that no sensitive data is in here
        since it will likely end up in some sort of public git repository.
    """
    try:
        db.session.add(Role("Owner"))
        db.session.add(Role("Regular"))
        db.session.add(User("admin", "admin@admin.com", "123"))
        db.session.add(User("second-user", "second-user@admin.com", "123"))
        for game in games_to_seed:
            db.session.add(Steam.add_game(game))
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print("Failed to perform seed")
        print(e)


def setup_test_database():
    MigrateCommand()
