# -*- coding: utf-8 -*-
"""
    These routes are for the more generic game-related features, like
    just adding games to the database. In the future, these should probably
    be updated via scheduled jobs.

    Missing features:
    * Not sure, don't think deleting games is particularly important?
"""
from flask import Blueprint, jsonify
from flask_cors import cross_origin
from server.service.steam import Steam
from server.model.game import game_schema, games_schema, Game

from flask_jwt_extended import jwt_required

game_blueprint = Blueprint("game", __name__, url_prefix="/api/game")


@game_blueprint.route("/", methods=["GET"])
@jwt_required
def get_games():
    game_list = Game.query.all()

    response = games_schema.jsonify(game_list)
    return response


@game_blueprint.route("/<slug>", methods=["GET"])
@jwt_required
def get_game(slug):
    game = Game.query.filter_by(slug=slug).first()

    if not game:
        return jsonify({"message": "Game doesnt exist in local database, add it first"})

    return game_schema.jsonify(game)


@game_blueprint.route("/wishlist/<steam_id>", methods=["POST"])
@jwt_required
def add_games_from_wishlist(steam_id):
    try:
        steam_id_list = Steam.get_wishlist(steam_id)
        number_of_games = 0
        for id in steam_id_list:
            print("adding {} to list".format(id))
            Steam.add_game(id)
            number_of_games += 1

        return jsonify(
            {"message": "Successfully added {} games".format(number_of_games)}
        )

    except Exception as e:
        return jsonify({"message": str(e)})


@game_blueprint.route("/add/<id>", methods=["POST"])
@jwt_required
def add_game(id):
    try:
        game = Steam.add_game(id)
        return game_schema.jsonify(game)
    except Exception as e:
        return jsonify({"message": str(e)})


@game_blueprint.route("/library", methods=["POST"])
@jwt_required
def fill_game_library():
    try:
        Steam.update_games_from_steam()
        return True
    except Exception as e:
        return jsonify({"message": str(e)})
