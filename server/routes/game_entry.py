# -*- coding: utf-8 -*-
"""
    Routes for the GameEntry objects, these are the bread and butter
    of the application.

    Missing features:
    * Removing a game entry
    * Removing a comment
    * A bunch of other features?
"""
from flask import Blueprint, jsonify, request
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy import exc
from datetime import datetime

from server.model.game_entry import GameEntry
from server.model.game_entry_rating import GameEntryRating
from server.model.game_entry_comment import GameEntryComment


game_entry_blueprint = Blueprint("game_entry", __name__, url_prefix="/api/game_entry")


@game_entry_blueprint.route("/<game_entry_id>/comment", methods=["POST"])
@jwt_required
def add_comment_to_game_entry(game_entry_id):
    try:
        current_user = get_jwt_identity()
        data = request.get_json()

        message = data.get("message", None)
        criterion = [message, len(data) == 1]

        if not all(criterion):
            return jsonify({"message": "Invalid POST-body"}), 400

        game_entry = GameEntry.query.filter_by(id=game_entry_id).first()

        if not game_entry:
            return (
                jsonify({"message": "That entry does not exist, what are you doing!?"}),
                404,
            )

        GameEntryComment(current_user, game_entry.id, message).save()
        return jsonify({"message": "Successfully commented"}), 200

    except Exception as e:
        return jsonify({"message": str(e)}), 500


@game_entry_blueprint.route("/<game_entry_id>/rate/<rating>", methods=["POST"])
@jwt_required
def add_rating_to_game(game_entry_id, rating):
    try:
        current_user = get_jwt_identity()

        rating = int(rating)

        if rating > 5:
            rating = 5

        if rating <= 0:
            rating = 1

        game_entry = GameEntry.query.filter_by(id=game_entry_id).first()

        if not game_entry:
            return (
                jsonify({"message": "That entry does not exist, what are you doing!?"}),
                404,
            )

        # has user already voted?
        game_rating = GameEntryRating.query.filter_by(
            user_id=current_user, game_entry_id=game_entry.id
        ).first()

        if not game_rating:
            GameEntryRating(current_user, game_entry.id, rating).save()
            return (
                jsonify(
                    {
                        "message": "You successfully rated a game",
                        "rating": rating,
                        "game": game_entry.game.name,
                    }
                ),
                200,
            )
        else:
            game_rating.rating = rating
            game_rating.updated_at = datetime.now()
            game_rating.update()
            return (
                jsonify(
                    {
                        "message": "Updated your rating",
                        "rating": rating,
                        "game": game_entry.game.name,
                    }
                ),
                200,
            )

    except exc.IntegrityError as e:
        return jsonify({"message": "Did you somehow fail at updating?"}), 500
    except Exception as e:
        return jsonify({"message": str(e)}), 500
