# -*- coding: utf-8 -*-
"""
    Routes for the list-management features of the application.

    Missing features:
    * Removing games from lists
    * Some sort of features regarding marking games as played
    * Also for which ones that are currently being played
"""
from flask import Blueprint, request, jsonify
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy import exc

from server.exceptions import SteamAppNotFound, EntityDoesNotExist, PermissionError
from server.model.list import list_schema, list_schemas, List
from server.model.game import Game
from server.model.list_role import ListRole
from server.model.game_entry import GameEntry, game_entry_schema
from server.model.user import User
from server.model.token import Token, token_schema
from server.service.role import RoleService
from server.service.steam import Steam
from server.service.list import ListService


list_blueprint = Blueprint("list", __name__, url_prefix="/api/list")


@list_blueprint.route("/", methods=["GET"])
@jwt_required
def get_lists():
    try:
        current_user = get_jwt_identity()
        lists = ListService.get_all(current_user)
        return list_schemas.jsonify(lists), 200
    except EntityDoesNotExist as e:
        return jsonify({"message": str(e)}), 404
    except PermissionError as e:
        return jsonify({"message": str(e)}), 401
    except Exception as e:
        return jsonify({"message": str(e)}), 500


@list_blueprint.route("/<list_slug>", methods=["GET"])
@jwt_required
def get_list(list_slug):
    try:
        current_user = get_jwt_identity()
        list = ListService.get(list_slug, current_user)
        return list_schema.jsonify(list), 200
    except EntityDoesNotExist as e:
        return jsonify({"message": str(e)}), 404
    except PermissionError as e:
        return jsonify({"message": str(e)}), 401
    except Exception as e:
        return jsonify({"message": str(e)}), 500


# TODO If everything else uses slugs for ids, is it worth keeping the delete as id.
@list_blueprint.route("/<id>", methods=["DELETE"])
@jwt_required
def delete_list(id):
    current_user = get_jwt_identity()
    ListService.delete(current_user, id)

    return jsonify("Successfully deleted list"), 200


@list_blueprint.route("/create", methods=["POST"])
@jwt_required
def create_list():
    current_user = get_jwt_identity()
    data = request.get_json()

    name = data.get("name", None)
    criterion = [name, len(data) == 1]

    if not all(criterion):
        return jsonify({"message": "Invalid POST-body"}), 400

    # Check if List exists first
    exists = ListService.exists(name)

    if exists:
        return jsonify({"message": "A list with that name already exists"}), 400

    list = List(name, 1, current_user).save()
    ListRole(current_user, list.id, RoleService.owner).save()
    return list_schema.jsonify(list), 201


@list_blueprint.route("/<list_slug>/<steam_id>", methods=["POST"])
@jwt_required
def add_game_to_list(list_slug, steam_id):
    try:
        current_user = get_jwt_identity()

        # Check if the list exists
        list = ListService.get(list_slug, current_user)

        game = Game.query.filter_by(steam_id=steam_id).first()

        if not game:
            # Add game ourselves
            game = Steam.add_game(steam_id)

        game_entry = GameEntry(current_user, list.id, game.id).save()
        return game_entry_schema.jsonify(game_entry)

    except exc.IntegrityError as e:
        return jsonify({"message": "Game has already been added to this list"}), 204
    except EntityDoesNotExist as e:
        return jsonify({"message": str(e)}), 404
    except SteamAppNotFound as e:
        return jsonify({"message": str(e)}), 400
    except Exception as e:
        return jsonify({"message": str(e)}), 500


# TODO MISSING TESTS
@list_blueprint.route("/<id>/<token>", methods=["POST"])
@jwt_required
def consume_invitation_for_list(id, token):
    try:
        current_user = get_jwt_identity()

        invite_list = ListService.get(id, current_user)
        print(invite_list)
        token = Token(1, invite_list.id).save()

        return token_schema.jsonify(token)
    except Exception as e:
        return jsonify({"message": str(e)}), 500


# TODO MISSING TESTS
@list_blueprint.route("/<id>/invite", methods=["POST"])
@jwt_required
def create_invitation_for_list(id):
    try:
        current_user = get_jwt_identity()
        invite_list = ListService.get(id, current_user)
        token = Token(1, invite_list.id).save()

        return token_schema.jsonify(token)
    except Exception as e:
        return jsonify({"message": str(e)}), 500


@list_blueprint.route("/<list_slug>/user/<user_id>", methods=["POST"])
@jwt_required
def add_user_to_list(list_slug, user_id):
    try:
        current_user = get_jwt_identity()
        ListService.add_user_to_list(current_user, user_id, list_slug)
        return jsonify({"message": "You successfully added the user to the list"}), 200

    except EntityDoesNotExist as e:
        return jsonify({"message": str(e)}), 404
    except PermissionError as e:
        return jsonify({"message": str(e)}), 401
    except Exception as e:
        return jsonify({"message": str(e)}), 500


@list_blueprint.route("/<list_slug>/user/<user_id>", methods=["DELETE"])
@jwt_required
def remove_user_from_list(list_slug, user_id):
    try:
        current_user = get_jwt_identity()

        # Check if list exists
        check_list = ListService.get(list_slug, current_user)
        if not check_list:
            return (
                jsonify(
                    {
                        "message": "That list does not exist, or you do not have access to it"
                    }
                ),
                404,
            )

        # Check that the current user has permissions
        check_list_role = ListRole.query.filter_by(
            user_id=current_user, list_id=check_list.id
        ).first()
        if not check_list_role:
            return (
                jsonify(
                    {
                        "message": "That list does not exist, or you do not have access to it"
                    }
                ),
                404,
            )

        if check_list_role.role_id != RoleService.owner:
            return (
                jsonify(
                    {"message": "You don't have access to remove users from this list"}
                ),
                401,
            )

        # Check that the user exists
        check_user = User.get_by_id(user_id)
        if not check_user:
            return (
                jsonify({"message": "The user you're trying to remove doesnt exist"}),
                404,
            )

        ListRole.query.filter_by(user_id=user_id, list_id=check_list.id).delete()
        return (
            jsonify(
                {
                    "message": "You successfully removed the user ({}) from the list".format(
                        user_id
                    )
                }
            ),
            200,
        )

    except Exception as e:
        print(e)
        return "", 404
