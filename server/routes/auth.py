# -*- coding: utf-8 -*-
"""
    The authentication part of the routes.

    Should probably contain some settings in the future, and perhaps
    additional routes to handle things like Discord usernames and maybe
    Steam user_ids?

    Currently missing:
    * Reset Password
    * Logout
"""
from flask import Blueprint, request, make_response, jsonify
from flask_jwt_extended import (
    jwt_optional,
    jwt_required,
    create_access_token,
    get_jwt_identity,
)

from server.model.user import User

auth_blueprint = Blueprint("auth", __name__, url_prefix="/api/auth")


@auth_blueprint.route("/login", methods=["POST"])
@jwt_optional
def login():
    if not request.is_json:
        return jsonify({"message": "Invalid post body"}), 400

    data = request.get_json()

    email = data.get("email", None)
    password = data.get("password", None)
    criterion = [password, email, len(data) == 2]

    if not all(criterion):
        return jsonify({"message": "Invalid credentials"}), 401
    try:
        user = User.query.filter_by(email=email).first()
        if user is not None and user.check_password(password):
            return (
                jsonify(
                    {
                        "status": "success",
                        "message": "Successfully logged in.",
                        "auth_token": create_access_token(identity=user, fresh=True),
                    }
                ),
                200,
            )
        else:
            return jsonify({"message": "Invalid credentials"}), 401
    except Exception as e:
        return (
            (
                jsonify(
                    {
                        "status": "fail",
                        "message": "Some error occurred. Please try again",
                        "error": str(e),
                    }
                )
            ),
            401,
        )


@auth_blueprint.route("/register", methods=["POST"])
def register():
    data = request.get_json()

    username = data.get("username", None)
    email = data.get("email", None)
    password = data.get("password", None)
    criterion = [username, password, email, len(data) == 3]

    if not all(criterion):
        return jsonify({"message": "Invalid credentials"})

    user = User.query.filter_by(email=email).first()

    if not user:
        try:
            user = User(username=username, password=password, email=email).save()

            user.token = create_access_token(identity=user)
            return (
                make_response(
                    jsonify(
                        {
                            "status": "success",
                            "message": "Successfully registered",
                            "auth_token": user.token,
                        }
                    )
                ),
                200,
            )
        except Exception as e:
            return (
                (
                    jsonify(
                        {
                            "status": "fail",
                            "message": "Some error occurred. Please try again",
                            "error": str(e),
                        }
                    )
                ),
                401,
            )

    else:
        return (
            make_response(
                jsonify({"status": "fail", "message": "User already exists."})
            ),
            400,
        )


@auth_blueprint.route("/me")
@jwt_required
def get_me():
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200
