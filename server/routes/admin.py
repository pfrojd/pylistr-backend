# -*- coding: utf-8 -*-
"""
    The admin-related routes, for sitewide settings and whatnot.
"""
from flask import Blueprint, jsonify
from server.model.user import User
from server.model.role import Role

from flask_jwt_extended import jwt_required, get_jwt_identity

admin_blueprint = Blueprint("admin", __name__, url_prefix="/api/admin")


@admin_blueprint.route("/settings/role", methods=["POST"])
@jwt_required
def add_list_role():
    """
        FIXME: This is probably not needed anymore since we have
        seeds for this type of operation.

        Should probably be extended so that it can add a new role.
    """
    try:
        current_user = get_jwt_identity()
        user = User.query.filter_by(id=current_user).first()

        if user and user.admin:
            Role("Owner").save()
            Role("Regular").save()
            return jsonify({"message": "Saved role settings"}), 200

        return "", 404
    except Exception as e:
        return "", 404


@admin_blueprint.route("/")
@jwt_required
def get_admin_status():
    """
        Naive implementation of admin checks, since they are just a boolean
        on the User object.
    """
    try:
        current_user = get_jwt_identity()

        user = User.query.filter_by(id=current_user).first()

        if user and user.admin:
            return jsonify({"message": "You're the admin"})

        return "", 404
    except Exception as e:
        return "", 404

