# -*- coding: utf-8 -*-
from flask import Flask, jsonify
from flask_cors import CORS
from server.extensions import bcrypt, jwt, db, migrate, ma
from server.exceptions import BadRequest, EntityDoesNotExist
from server.seed import seed_db

from server.routes.auth import auth_blueprint
from server.routes.list import list_blueprint
from server.routes.game import game_blueprint
from server.routes.game_entry import game_entry_blueprint
from server.routes.admin import admin_blueprint


def create_app(active_config):
    app = Flask(__name__)
    CORS(app, resources={r"/*": {"origins": "*"}}, support_credentials=True)
    # read the config file
    app.config.from_object(active_config)
    app.secret_key = app.config.get("APP_SECRET")

    register_extensions(app)
    register_blueprints(app)
    register_handlers(app)
    register_commands(app)

    # # Serve HTML from here
    # @app.route("/", defaults={"path": ""})
    # @app.route("/<path:path>")
    # def catch_all(path):
    #     return jsonify(foo="bar", test="hello")

    return app


def register_extensions(app):
    """
        Register Flask Extensions
    """

    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    jwt.init_app(app)
    ma.init_app(app)


def register_blueprints(app):
    """
        Register Flask Blueprints
    """
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(list_blueprint)
    app.register_blueprint(game_blueprint)
    app.register_blueprint(game_entry_blueprint)
    app.register_blueprint(admin_blueprint)


def register_handlers(app):
    """
        In order to get Flask to properly react to our exceptions
        we have to register them in the flask errorhandler.
    """

    # Don't use this yet.
    #
    # @app.errorhandler(Exception)
    # def handle_generic_error(error):
    #     """
    #         This error handler is here to ensure that if we at any point
    #         have a generic exception, we should at least have some sort of response.
    #     """
    #     response = jsonify({"error": "Something went wrong"})
    #     response.status_code = 500
    #     return response

    @app.errorhandler(BadRequest)
    def handle_bad_request(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response

    @app.errorhandler(EntityDoesNotExist)
    def handle_entity_does_not_exist(error):
        response = jsonify(error.to_dict())
        response.status_code = error.status_code
        return response


def register_commands(app):
    """
        This is used to register `commands` for flask.

        See http://flask.pocoo.org/docs/1.0/cli/#custom-commands for more info.

        To run the seed()-method below, use `flask seed` as a command in your CLI.
    """

    @app.cli.command()
    def seed():
        seed_db()

