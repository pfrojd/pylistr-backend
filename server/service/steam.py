# -*- coding: utf-8 -*-
"""
    This module is responsible for dealing with Steam requests.
    Anything steam-related should be added here.
"""

import requests
import re
import json

from server.model.category import Category
from server.model.genre import Genre
from server.model.game import Game
from server.model.steam_game import SteamGame
from server.exceptions import SteamAppNotFound, SteamGameAlreadyAdded
from bs4 import BeautifulSoup


def safe_get_value(dct, *keys):
    for key in keys:
        try:
            dct = dct[key]
        except KeyError:
            return None
    return dct


def get_early_access_flag(obj):
    early_access = False
    genres = safe_get_value(obj, "genres")
    for genre in genres:
        if genre.get("id") == 70 or genre.get("description") == "Early Access":
            early_access = True

    return early_access


class Steam:
    @staticmethod
    def add_game(id):
        if not id:
            raise Exception("Bad id passed to SteamService")

        game = Game.query.filter_by(steam_id=id).first()

        if not game:
            r = requests.get(
                f"https://store.steampowered.com/api/appdetails/?appids={id}"
            )
            if r.status_code != 200:
                raise SteamAppNotFound("Could not fetch that game")

            # Traverse JSON to actual game data
            steam_game = r.json().get(str(id)).get("data")

            # FIXME: More error handling in here

            # print(steam_game)
            game = Game(
                safe_get_value(steam_game, "name"),
                safe_get_value(steam_game, "steam_appid"),
                safe_get_value(steam_game, "price_overview", "final_formatted"),
                safe_get_value(steam_game, "is_free"),
                safe_get_value(steam_game, "recommendations", "total"),
                safe_get_value(steam_game, "metacritic", "score"),
                safe_get_value(steam_game, "release_date", "date"),
                not safe_get_value(steam_game, "release_date", "coming_soon"),
                get_early_access_flag(steam_game),
                steam_game,
            )

            # Fetch categories and populate game with relevant ones.
            all_categories = Category.query.all()
            raw_categories = safe_get_value(steam_game, "categories")
            categories = []

            for new_category in raw_categories:
                c = safe_get_value(new_category, "description")
                exists = list(filter(lambda item: item.name == c, all_categories))
                if exists:
                    category = exists[0]
                else:
                    category = Category(c).save()

                categories.append(category)

            game.categories = categories

            # Fetch genres and populate game with relevant ones.
            all_genres = Genre.query.all()
            raw_genres = safe_get_value(steam_game, "genres")
            genres = []

            for new_genre in raw_genres:
                g = safe_get_value(new_genre, "description")
                exists = list(filter(lambda item: item.name == g, all_genres))
                if exists:
                    genre = exists[0]
                else:
                    genre = Genre(g).save()

                genres.append(genre)

            game.genres = genres
            game.save()

            return game

        raise SteamGameAlreadyAdded("Game already added to database")

    @staticmethod
    def get_wishlist(steam_user_id):
        if not steam_user_id:
            raise Exception("Invalid ID passed to get_wishlist")

        url = "http://steamcommunity.com/profiles/{}/wishlist".format(steam_user_id)
        response = requests.get(url)
        soup = BeautifulSoup(response.content, "html.parser")
        container = soup.select(
            ".responsive_page_template_content > script:first-child"
        )[0]
        # print(container)
        all_games = []
        test = re.search(r"\[(.*)\]", str(container))

        if not test:
            raise Exception("Empty wishlist from steam :(")

        wishlist_json = json.loads(test.group(0))

        for game in wishlist_json:
            game_id = game["appid"]
            all_games.append(game_id)

        return all_games

    @staticmethod
    def update_games_from_steam():

        url = "https://api.steampowered.com/ISteamApps/GetAppList/v2/"
        response = requests.get(url)

        if response.status_code != 200:
            raise SteamAppNotFound("Could not fetch complete game list")

        list = response.json().get("applist").get("apps")

        for game in list:
            print("inserting game {}".format(game.get("name")))
            SteamGame(game.get("name"), game.get("appid")).save()

    @staticmethod
    def search_game(query):

        query = SteamGame.__ts_vector__.match(query, postgresql_regconfig="english")
        games = query.all()

        return games
