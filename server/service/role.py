# -*- coding: utf-8 -*-
"""
    This looks simple for now but is just a shortcut to avoid having to write
    `1` or `2` in the code whenever referring to a regular user or an owner.
"""


class RoleService:
    owner = 1
    user = 2
