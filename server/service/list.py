# -*- coding: utf-8 -*-
"""
    Since Lists are a bit of a special snowflake, this service is set up to
    avoid doing some extra permissions related checks whenever we try to fetch lists.

    Also validation is nice.

    At some point, more of the routes within `server/routes/list.py` should
    probably be here.
"""

from slugify import slugify
from server.exceptions import EntityDoesNotExist, PermissionError

from server.model.list import List
from server.model.list_role import ListRole
from server.model.user import User
from server.model.token import Token
from server.service.role import RoleService


class ListService:
    @staticmethod
    def exists(name):
        """
            Will check if list already exists.
            Decide later if this is a security risk (that we don't check permissions)
        """
        slug = slugify(name)
        check_list = List.get_by_slug(slug)
        if not check_list:
            return False
        else:
            return True

    @staticmethod
    def get(list_slug, user_id):
        """
            Will check if List exists, then check if the User has
            access to that particular list. If not, raise a generic error message.
        """
        # Check if the list exists
        check_list = List.get_by_slug(list_slug)
        if not check_list:
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it", 404
            )

        # Check if the user exists on the list
        check_list_role = ListRole.query.filter_by(
            user_id=user_id, list_id=check_list.id
        ).first()
        if not check_list_role:
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it", 404
            )

        return check_list

    @staticmethod
    def get_all(user_id):
        """
            When fetching all lists of a particular user, it's more useful
            to first fetch all of the lists we know they have access to,
            and only after fetch the actual lists.

            Not much validation here, just return empty lists.
        """
        all_lists = (
            ListRole.query.with_entities(ListRole.list_id)
            .filter_by(user_id=user_id)
            .all()
        )

        if not all_lists:
            return []

        lists = List.query.filter(List.id.in_(all_lists)).all()
        return lists

    @staticmethod
    def delete(user_id, list_id):
        """
            A delete method for a list, will make sure all additional
            permissions are fulfilled before deleting.
        """
        # Check if the list exists
        check_list = List.query.filter_by(id=list_id).first()
        if not check_list:
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it", 404
            )

        # Check if the user exists on the list
        check_list_role = ListRole.query.filter_by(
            user_id=user_id, list_id=list_id
        ).first()
        if not check_list_role:
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it", 404
            )

        if check_list_role.role_id != RoleService.owner:
            raise PermissionError("You don't have permission to delete this list")

        check_list.delete()

    @staticmethod
    def add_user_to_list(current_user, user_id, list_slug):
        check_list = List.get_by_slug(list_slug)
        if not check_list:
            print("That list does not exist, or you do not have access to it")
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it"
            )

        # Check that the current user has permissions
        check_list_role = ListRole.query.filter_by(
            user_id=current_user, list_id=check_list.id
        ).first()
        if not check_list_role:
            print(
                "That list does not exist, or you do not have access to it - check_list"
            )
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it"
            )

        if check_list_role.role_id != RoleService.owner:
            print("You don't have access to add users to this list")
            raise PermissionError("You don't have access to add users to this list")

        # TODO: Shouldn't this be done earlier?
        # Check that the user exists
        check_user = User.get_by_id(user_id)
        if not check_user:
            print("The user you're trying to add doesnt exist")
            raise EntityDoesNotExist("The user you're trying to add doesnt exist")

        print("saving role")
        ListRole(user_id, check_list.id, RoleService.user).save()

    @staticmethod
    def add_user_to_list_by_token(user_id, token, list_id):
        check_token = Token.query.filter_by(token=token).first()
        if not check_token:
            raise EntityDoesNotExist("This token does not exist")

        check_list = List.get_by_id(list_id)
        if not check_list:
            raise EntityDoesNotExist(
                "That list does not exist, or you do not have access to it"
            )

        # TODO: Shouldn't this be done earlier?
        # Check that the user exists
        check_user = User.get_by_id(user_id)
        if not check_user:
            raise EntityDoesNotExist("The user you're trying to add doesnt exist")

        ListRole(user_id, list_id, RoleService.user).save()
