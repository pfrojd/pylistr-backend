from sqlalchemy import UniqueConstraint

from server.database import db, FancyModel
from server.extensions import ma


"""
    Roles consist of an id and a name.

    There's also a UniqueConstraint making sure that each Role can only exist once.
"""


class Role(FancyModel, db.Model):
    __tablename__ = "role"

    role_name = db.Column(db.String(256), nullable=False)
    UniqueConstraint(role_name, name="UniqueRole")

    def __repr__(self):
        return "<id {}>".format(self.id)

    def __init__(self, role_name):
        db.Model.__init__(self, role_name=role_name)


class RoleSchema(ma.Schema):
    role_name = ma.Str()
