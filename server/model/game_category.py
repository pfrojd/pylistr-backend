from datetime import datetime
from server.database import FancyModel, db
from server.extensions import ma

"""
    Junction model for M:N Game/Category relation.
"""


class GameCategory(db.Model):

    __tablename__ = "game_category"

    game_id = db.Column(db.Integer, db.ForeignKey("game.id", ondelete="CASCADE"), primary_key=True)
    category_id = db.Column(db.Integer, db.ForeignKey(
        "category.id", ondelete="CASCADE"), primary_key=True)

    # def __init__(self, game_id, category_id):
    #     """Create instance."""
    #     db.Model.__init__(
    #         self,
    #         game_id=game_id,
    #         category_id=category_id,
    #     )

    def __repr__(self):
        return "<id {}>".format(self.id)
