from server.database import db, FancyModel

"""
    ListRole is the implementation of roles on the List entity.
    It's related to a user, a list and a specific role_id, which means that
    a single user can have multiple roles across different lists.

    Roles in this case is only limited to being a user and an admin, but
    could be extended in the future.
"""


class ListRole(FancyModel, db.Model):
    __tablename__ = "list_role"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    list_id = db.Column(db.Integer, db.ForeignKey("list.id"))
    role_id = db.Column(db.Integer, db.ForeignKey("role.id"))

    def __repr__(self):
        return "<id {}>".format(self.id)

    def __init__(self, user_id, list_id, role_id):
        db.Model.__init__(self, user_id=user_id, list_id=list_id, role_id=role_id)
