from server.database import db
from sqlalchemy.sql import func
from sqlalchemy.dialects.postgresql import TSVECTOR

"""
    These models are just here for lookup regarding search etc.
    The TSVECTOR column is updated automagically thanks to a trigger on the table whenever
    a new game is inserted or updated.

    These games are loaded from the giant JSON endpoint that Steam provides, and should probably
    be updated once a day.
"""


def to_tsvector_ix(*columns):
    s = " || ' ' || ".join(columns)
    return func.to_tsvector('english', db.text(s))


class SteamGame(db.Model):
    __tablename__ = "steam_game"

    steam_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    search = db.Column(TSVECTOR)

    def __repr__(self):
        return "<id {}>".format(self.id)

    def __init__(self, name, steam_id):
        db.Model.__init__(self, name=name, steam_id=steam_id)
