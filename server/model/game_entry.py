from datetime import datetime
from sqlalchemy import UniqueConstraint
from server.database import FancyModel, db
from server.extensions import ma

from server.model.game import GameSchema
from server.model.user import UserSchema
from server.model.game_entry_rating import GameEntryRatingSchema
from server.model.game_entry_comment import GameEntryCommentSchema

"""
    GameEntry is the core that binds the application together in its current state.
    GameEntries exists in a list, but are tied to a User (who added it) and to the Game.

    The Game entity is kept separate.

    It contains a UniqueConstraint to make sure that a game is only added once to a list.
    A GameEntry also contains Comments and Ratings.
"""


class GameEntry(FancyModel, db.Model):
    __tablename__ = "game_entry"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    list_id = db.Column(db.Integer, db.ForeignKey("list.id"))
    game_id = db.Column(db.Integer, db.ForeignKey("game.id"))

    user = db.relationship("User", foreign_keys=[user_id])
    list = db.relationship("List", foreign_keys=[list_id])
    game = db.relationship("Game", foreign_keys=[game_id])

    ratings = db.relationship("GameEntryRating", cascade="all, delete-orphan")
    comments = db.relationship("GameEntryComment", cascade="all, delete-orphan")

    UniqueConstraint(user_id, list_id, game_id, name="UniqueGameEntry")

    def __init__(self, user, list, game):
        """Create instance."""
        db.Model.__init__(
            self,
            user_id=user,
            list_id=list,
            game_id=game,
            created_at=datetime.now(),
            updated_at=datetime.now(),
        )

    def __repr__(self):
        return "<id {}>".format(self.id)


class GameEntrySchema(ma.Schema):
    id = ma.Integer()
    created_at = ma.DateTime()
    updated_at = ma.DateTime()
    game = ma.Nested(GameSchema)
    user = ma.Nested(UserSchema)
    ratings = ma.Nested(GameEntryRatingSchema, many=True)
    comments = ma.Nested(GameEntryCommentSchema, many=True)


game_entry_schema = GameEntrySchema()
game_entry_schemas = GameEntrySchema(many=True)
