from datetime import datetime
from sqlalchemy import UniqueConstraint

from server.database import FancyModel, db
from server.extensions import ma
from server.model.user import UserShortSchema

"""
    GameEntryRating is a model for showing a single rating of a single GameEntry
    It contains a UniqueConstraint making sure that you're only able to have one rating.

    Currently the service implementation makes sure that we update the current rating,
    but attempting to do it outside, the database will throw an exception.
"""


class GameEntryRating(FancyModel, db.Model):

    __tablename__ = "game_entry_rating"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)
    game_entry_id = db.Column(
        db.Integer, db.ForeignKey("game_entry.id"), nullable=False
    )
    rating = db.Column(db.Integer, nullable=False)

    user = db.relationship("User", foreign_keys=[user_id])

    UniqueConstraint(user_id, game_entry_id, name="UniqueGameEntryRatingPerUser")

    def __init__(self, user, game_entry, rating):
        self.validate_rating(rating)
        db.Model.__init__(
            self,
            user_id=user,
            game_entry_id=game_entry,
            rating=rating,
            created_at=datetime.now(),
            updated_at=datetime.now(),
        )

    def validate_rating(self, rating):
        if rating > 5:
            self.rating = 5

        if rating < 1:
            self.rating = 1

    def __repr__(self):
        return "<id {}>".format(self.id)


class GameEntryRatingSchema(ma.Schema):
    user = ma.Nested(UserShortSchema)
    rating = ma.Integer()
    updated_at = ma.DateTime()
