from datetime import datetime
from server.database import FancyModel, db
from server.extensions import ma

"""
    Junction model for M:N Game/Genre relation.
"""


class GameGenre(db.Model):

    __tablename__ = "game_genre"

    game_id = db.Column(db.Integer, db.ForeignKey("game.id", ondelete="CASCADE"), primary_key=True)
    genre_id = db.Column(db.Integer, db.ForeignKey(
        "genre.id", ondelete="CASCADE"), primary_key=True)

    def __repr__(self):
        return "<id {}>".format(self.id)
