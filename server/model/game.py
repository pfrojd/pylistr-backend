from server.database import db, FancyModel
from server.extensions import ma
from sqlalchemy.dialects.postgresql import JSON
from server.model.game_genre import GameGenre
from server.utils import validate_datetime
from slugify import slugify

"""
    Really need to figure out if it's a good idea to cache Steam games in the
    application or if we should just always go by live data. The benefit of not
    going live by Steam means we can do better categorization and filtering
    in the application as we do not need to lean on their implementation.

    See the SteamGame model for the entity that represents the large library of games
    Steam has available.

    The Game Model is only added once a Game has been added to a list, because only once
    that happens does the application care about the Game-related data.

    As long as the Game exists on any list, it should be updated by the application, and
    this is why it's being kept separate from the SteamGame implementation.
"""


class Game(FancyModel, db.Model):
    __tablename__ = "game"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    name = db.Column(db.String(256), nullable=False)
    steam_id = db.Column(db.Integer, unique=True, nullable=False)
    price_formatted = db.Column(db.String(256))
    is_free = db.Column(db.Boolean)
    recommendations = db.Column(db.Integer, nullable=True)
    metacritic = db.Column(db.Integer)
    release_date = db.Column(db.DateTime, nullable=True)
    is_released = db.Column(db.Boolean)
    is_early_access = db.Column(db.Boolean)
    data = db.Column(JSON)
    slug = db.Column(db.String(256), nullable=False)

    categories = db.relationship(
        "Category",
        secondary="game_category",
        backref=db.backref("game"),
        cascade="delete",
    )

    genres = db.relationship(
        "Genre", secondary="game_genre", backref=db.backref("game"), cascade="delete"
    )

    def __repr__(self):
        return "<id {}>".format(self.id)

    def __init__(
        self,
        name,
        steam_id,
        price_formatted,
        is_free,
        recommendations,
        metacritic,
        release_date,
        is_released,
        is_early_access,
        data,
    ):
        release_date = validate_datetime(release_date)
        slug = slugify(name)
        db.Model.__init__(
            self,
            name=name,
            steam_id=steam_id,
            price_formatted=price_formatted,
            is_free=is_free,
            recommendations=recommendations,
            metacritic=metacritic,
            release_date=release_date,
            is_released=is_released,
            is_early_access=is_early_access,
            data=data,
            slug=slug,
        )


class SteamGamePrice(ma.Schema):
    currency = ma.Str()
    discount_percent = ma.Integer()
    final = ma.Integer()
    final_formatted = ma.String()
    initial = ma.Integer()
    initial_formatted = ma.String()


class SteamGame(ma.Schema):
    about_the_game = ma.Str()
    background = ma.Url()
    detailed_description = ma.Str()
    developers = ma.List(ma.Str())
    header_image = ma.Url()
    name = ma.Str()
    steam_appid = ma.Integer()
    short_description = ma.Str()


class SteamGameShort(ma.Schema):
    about_the_game = ma.Str()
    name = ma.Str()
    price_overview = ma.Nested(SteamGamePrice)


class GameGenreSchema(ma.Schema):
    id = ma.Integer()
    name = ma.Str()


class GameCategorySchema(ma.Schema):
    id = ma.Integer()
    name = ma.Str()


class GameSchema(ma.Schema):
    id = ma.Integer()
    name = ma.Str()
    steam_id = ma.Integer()
    price_formatted = ma.Str()
    is_free = ma.Boolean()
    recommendations = ma.Integer()
    metacritic = ma.Integer()
    release_date = ma.DateTime()
    is_released = ma.Boolean()
    is_early_access = ma.Boolean()
    categories = ma.List(ma.Nested(GameCategorySchema))
    genres = ma.List(ma.Nested(GameGenreSchema))
    data = ma.Nested(SteamGame)
    slug = ma.Str()


steam_game_schema = SteamGame()
game_schema = GameSchema()
games_schema = GameSchema(many=True)
