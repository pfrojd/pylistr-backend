from server.database import FancyModel, db

"""
    A genre for games, such as Free to Play, Action, MMO etc.
"""


class Genre(FancyModel, db.Model):

    __tablename__ = "genre"

    name = db.Column(db.String(256), nullable=False)
    games = db.relationship("Game", secondary="game_genre", backref=db.backref("genre"))

    def __init__(self, name):
        """Create instance."""
        db.Model.__init__(
            self,
            name=name
        )

    def __repr__(self):
        return "<id {}>".format(self.id)
