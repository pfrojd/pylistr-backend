from enum import Enum
from uuid import uuid4
from datetime import datetime, timedelta
from sqlalchemy.dialects.postgresql import UUID

from server.database import db, FancyModel
from server.extensions import ma


"""
    Token
"""


class Token(FancyModel, db.Model):
    __tablename__ = "token"

    token = db.Column(UUID(as_uuid=True), default=uuid4)
    value = db.Column(db.Integer)
    type = db.Column(db.Integer, default=1)
    expiration = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return "<id {}>".format(self.id)

    @staticmethod
    def create_expiration_date(days):
        return datetime.now() + timedelta(days=days)

    def __init__(self, type, value):
        expiration = Token.create_expiration_date(7)
        db.Model.__init__(self, type=type, value=value, expiration=expiration)


class TokenType(Enum):
    INVITE_TOKEN = 1


class TokenSchema(ma.Schema):
    token = ma.Str()


token_schema = TokenSchema()
