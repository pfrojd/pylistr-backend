from server.database import FancyModel, db
from server.extensions import ma
from server.model.game_entry import GameEntrySchema
from slugify import slugify

"""
    Lists contain multiple GameEntry entities but also contain references
    to the ListRole table to maintain authorization across the list.

    Lists currently have a type, which means that lists could be different
    in the future.
"""


class List(FancyModel, db.Model):

    __tablename__ = "list"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    name = db.Column(db.String(256), nullable=False, unique=True)
    type = db.Column(db.Integer, nullable=False)
    game_entries = db.relationship("GameEntry", cascade="all, delete-orphan")
    list_role = db.relationship("ListRole", cascade="all, delete-orphan")
    slug = db.Column(db.String(256), nullable=False)

    def __init__(self, name, type, user_id):
        slug = slugify(name)
        db.Model.__init__(self, name=name, type=type, slug=slug)


class ListSchema(ma.Schema):
    id = ma.Integer()
    slug = ma.Str()
    name = ma.Str()
    created_at = ma.Str()
    updated_at = ma.Str()
    game_entries = ma.Nested(GameEntrySchema, many=True)


list_schema = ListSchema()
list_schemas = ListSchema(many=True)
