import datetime

from server.extensions import bcrypt, ma
from server.database import FancyModel, db

"""
    Users in the system have a username and email, both with a UniqueConstraint.
    Passwords are hashed and salted.

    Currently has a -NAIVE- implementation of being an admin across the board
    as a simple boolean.

    Users should be able to add their SteamID to keep things in sync in the future.
"""


class User(FancyModel, db.Model):

    __tablename__ = "user"

    username = db.Column(db.String(255), unique=True, nullable=False)
    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    admin = db.Column(db.Boolean, default=False)
    steam_id = db.Column(db.Text, nullable=True)
    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    token: str = ""

    def __init__(self, username, email, password=None, **kwargs):
        """Create instance."""
        db.Model.__init__(
            self,
            username=username,
            email=email,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
            **kwargs
        )
        if password:
            self.set_password(password)
        else:
            self.password = None

    def set_password(self, password):
        """Set password."""
        # postgreSQL specific stuff.
        pwhash = bcrypt.generate_password_hash(password.encode("utf8"))
        self.password = pwhash.decode("utf8")

    def check_password(self, value):
        """Check password."""
        return bcrypt.check_password_hash(self.password, value)

    def __repr__(self):
        """Represent instance as a unique string."""
        return "<User({username!r})>".format(username=self.username)


class UserSchema(ma.Schema):
    username = ma.Str()
    email = ma.Email()
    """ Only load password, never send """
    password = ma.Str(load_only=True)
    """ Only send these fields, never accept them """
    created_at = ma.Str(dump_only=True)
    updated_at = ma.Str(dump_only=True)


class UserShortSchema(ma.Schema):
    username = ma.Str()


user_schema = UserSchema()
user_schemas = UserSchema(many=True)
