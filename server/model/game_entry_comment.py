from datetime import datetime
from server.database import FancyModel, db
from server.extensions import ma

"""
    GameEntryComment is a single comment for a single GameEntry, it has relations
    to a user and a GameEntry.

    A user can create multiple comments on a single GameEntry.
    For now, comments cannot be updated (and probably don't need a timestamp for updated_at)
"""


class GameEntryComment(FancyModel, db.Model):

    __tablename__ = "game_entry_comment"

    created_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    updated_at = db.Column(db.DateTime, nullable=False, server_default=db.func.now())
    message = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    game_entry_id = db.Column(
        db.Integer, db.ForeignKey("game_entry.id"), nullable=False
    )

    user = db.relationship("User", foreign_keys=[user_id])

    def __init__(self, user, game_entry, message):
        """Create instance."""
        db.Model.__init__(
            self,
            user_id=user,
            game_entry_id=game_entry,
            message=message,
            created_at=datetime.now(),
            updated_at=datetime.now(),
        )

    def __repr__(self):
        return "<id {}>".format(self.id)


class GameEntryCommentSchema(ma.Schema):
    id = ma.Integer()
    created_at = ma.DateTime()
    updated_at = ma.DateTime()
    message = ma.Str()
