from datetime import datetime
from server.database import FancyModel, db
from server.extensions import ma
from server.model.game_category import GameCategory

"""
    A category for games, such as Single-Player, Co-op etc.
"""


class Category(FancyModel, db.Model):

    __tablename__ = "category"

    name = db.Column(db.String(256), nullable=False)
    games = db.relationship("Game", secondary="game_category", backref=db.backref("category"))

    def __init__(self, name):
        """Create instance."""
        db.Model.__init__(
            self,
            name=name
        )

    def __repr__(self):
        return "<id {}>".format(self.id)
