"""
    Some generic exceptions the application could use, the main point
    being to ensure that proper status_codes are returned when an exception is raised.

    These exceptions are then registed under `server/application.py` to make sure that
    Flask properly picks up the appropriate error.

    Shamefully copied from: http://flask.pocoo.org/docs/1.0/patterns/apierrors/
"""


class BaseException(Exception):
    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv


class EntityDoesNotExist(BaseException):
    status_code = 404


class PermissionError(BaseException):
    status_code = 401


class BadRequest(BaseException):
    status_code = 400


class AuthenticationError(BaseException):
    status_code = 401


class SteamAppNotFound(BaseException):
    status_code = 400


class SteamGameAlreadyAdded(BaseException):
    status_code = 204
