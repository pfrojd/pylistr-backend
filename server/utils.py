from dateutil import parser
from server.model.user import User


def jwt_identity(payload):
    """
        This defines how we do our user lookup for JWT
    """
    return User.get_by_id(payload)


def identity_loader(user):
    """
        This defines how we extract the id used for the token
        from the user object.
    """
    return user.id


def validate_datetime(dt):
    try:
        return parser.parse(dt)
    except Exception as e:
        return None
