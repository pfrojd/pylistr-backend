#!flask/bin/python
"""
    Managing Migrations

    This is not part of the application, but is something executed
    from commandline in order to manage database migrations.

    $ flask db migrate
    Will create a new version of the database, given the available models.

    $ flask db upgrade
    Will upgrade your local database to the latest version, running each of the migrations.

    $ flask db downgrade
    Will downgrade your local database to the previous version.

    Example:

    Adding table X to the database.
    1. Create a new model for the table, and define all columns (see other models for examples)
    2. Run the migrate command
    3. Run the upgrade command.
    4. You now have your new table.
"""
import os

from dotenv import load_dotenv
from server.application import create_app

# activate dotenv which will read our .env file with secrets
load_dotenv()
active_config = os.environ["APP_SETTINGS"]
app = create_app(active_config)

if __name__ == "__main__":
    app.run()
